<?php

namespace App\Entity;

use App\Repository\CurriculumRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CurriculumRepository::class)
 * @ApiResource()
 */
class Curriculum
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read"})
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="curriculums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Experience::class, inversedBy="curricula")
     * @Groups({"user:read"})
     */
    private $experiences;

    /**
     * @ORM\ManyToMany(targetEntity=Formation::class, inversedBy="curricula")
     * @Groups({"user:read"})
     */
    private $formations;

    /**
     * @ORM\ManyToMany(targetEntity=Passion::class, inversedBy="curricula")
     * @Groups({"user:read"})
     */
    private $passions;

    /**
     * @ORM\ManyToMany(targetEntity=Competence::class, inversedBy="curricula")
     * @Groups({"user:read"})
     */
    private $competences;

    /**
     * @ORM\ManyToMany(targetEntity=Langues::class, inversedBy="curricula")
     * @Groups({"user:read"})
     */
    private $langues;

    /**
     * @ORM\ManyToMany(targetEntity=Permis::class, inversedBy="curricula")
     * @Groups({"user:read"})
     */
    private $permis;

    public function __construct()
    {
        $this->experiences = new ArrayCollection();
        $this->competences = new ArrayCollection();
        $this->langues = new ArrayCollection();
        $this->permis = new ArrayCollection();
        $this->formations = new ArrayCollection();
        $this->passions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Experience[]
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences[] = $experience;
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        $this->experiences->removeElement($experience);

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormation(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        $this->formations->removeElement($formation);

        return $this;
    }

    /**
     * @return Collection|Passion[]
     */
    public function getPassion(): Collection
    {
        return $this->passions;
    }

    public function addPassion(Passion $passion): self
    {
        if (!$this->passions->contains($passion)) {
            $this->passions[] = $passion;
        }

        return $this;
    }

    public function removePassion(Passion $passion): self
    {
        $this->passions->removeElement($passion);

        return $this;
    }

    /**
     * @return Collection|Competence[]
     */
    public function getCompetences(): Collection
    {
        return $this->competences;
    }

    public function addCompetence(Competence $competence): self
    {
        if (!$this->competences->contains($competence)) {
            $this->competences[] = $competence;
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): self
    {
        $this->competences->removeElement($competence);

        return $this;
    }

    /**
     * @return Collection|Langues[]
     */
    public function getLangues(): Collection
    {
        return $this->langues;
    }

    public function addLangue(Langues $langue): self
    {
        if (!$this->langues->contains($langue)) {
            $this->langues[] = $langue;
        }

        return $this;
    }

    public function removeLangue(Langues $langue): self
    {
        $this->langues->removeElement($langue);

        return $this;
    }

    /**
     * @return Collection|Permis[]
     */
    public function getPermis(): Collection
    {
        return $this->permis;
    }

    public function addPermi(Permis $permi): self
    {
        if (!$this->permis->contains($permi)) {
            $this->permis[] = $permi;
        }

        return $this;
    }

    public function removePermi(Permis $permi): self
    {
        $this->permis->removeElement($permi);

        return $this;
    }
}
