<?php

namespace App\Entity;

use App\Repository\PassionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PassionRepository::class)
 * @ApiResource()
 */
class Passion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read"})
     */
    private $passion;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="hobies")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Curriculum::class, mappedBy="passions")
     */
    private $curricula;

    public function __construct()
    {
        $this->curricula = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPassion(): ?string
    {
        return $this->passion;
    }

    public function setPassion(string $passion): self
    {
        $this->passion = $passion;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Curriculum[]
     */
    public function getCurricula(): Collection
    {
        return $this->curricula;
    }

    public function addCurriculum(Curriculum $curriculum): self
    {
        if (!$this->curricula->contains($curriculum)) {
            $this->curricula[] = $curriculum;
            $curriculum->addPassion($this);
        }

        return $this;
    }

    public function removeCurriculum(Curriculum $curriculum): self
    {
        if ($this->curricula->removeElement($curriculum)) {
            $curriculum->removePassion($this);
        }

        return $this;
    }
}
