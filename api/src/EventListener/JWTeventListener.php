<?php

namespace App\EventListener;

use App\Entity\Competence;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class JWTeventListener
{

    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event) 
    {
        $data = $event->getData();
        $user = $event->getUser();

        if(!$user instanceof UserInterface){
            return;
        }

        if($user instanceof User){
            
                $data += array(
                'id' => $user->getId(),
                'email' => $user->getUsername(),
                'nom' => $user->getNom(),
                'prenom' => $user->getPrenom(),
                'roles' => $user->getRoles(),
            );

            $event->setData($data);
        } 
    }
}