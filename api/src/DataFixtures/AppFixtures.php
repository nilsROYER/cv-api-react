<?php

namespace App\DataFixtures;

use App\Entity\Competence;
use App\Entity\Experience;
use App\Entity\Formation;
use App\Entity\Langues;
use App\Entity\Passion;
use App\Entity\Permis;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setNom('bob')
        ->setPrenom('bob')
        ->setEmail('test@test.fr')
        ->setPassword($this->encoder->encodePassword($user, 'test'))
        ->setCreatedAt(new DateTime());

        $manager->persist($user);

        for($c = 0; $c < 10; $c++){
            $competence = (new Competence())
            ->setNom("compétence " . $c)
            ->setDescription("une description")
            ->setUser($user);

            $manager->persist($competence);
        }

        for($f = 1; $f < 10; $f++){
            $formations = (new Formation())
            ->setTitre("titre n°" . $f)
            ->setEtablissement("azcaban" . $f)
            ->setUser($user);

            $manager->persist($formations);
        }

        for($e = 1; $e < 10; $e++) {
            $experience = (new Experience())
            ->setPoste(sprintf("random %s", $e))
            ->setDescription(sprintf("description du travail effectuer n°%s", $e))
            ->setEmployeur(sprintf("mister %s", $e))
            ->setUser($user);

            $manager->persist($experience);
        }

        $languages = [
            "en",
            "fr",
            "es",
        ];
        for($l = 0; $l < count($languages); $l++) {
            $langue = (new Langues())
            ->setLangue($languages[$l])
            ->setLevel(2)
            ->setUser($user);

            $manager->persist($langue);
        }

        for($p = 0; $p < 4; $p++) {
            $hobie = (new Passion())
            ->setPassion(sprintf("une passion n°%s", $p))
            ->setUser($user);

            $manager->persist($hobie);
        }

        for($miper = 0; $miper < 2; $miper++){
            $permi = (new Permis())
            ->setNom(sprintf("un permis %s", $miper))
            ->setLevel("b")
            ->setVehicule(true)
            ->setUser($user);

            $manager->persist($permi);
        } 

        
        $manager->flush();
    }
}
