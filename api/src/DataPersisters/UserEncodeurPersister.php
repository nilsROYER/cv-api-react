<?php 

namespace App\DataPersisters;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserEncodeurPersister implements DataPersisterInterface 
{

    private $encoder;

    private $em;

    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        $this->encoder = $encoder;
        $this->em = $em;
    }
     /**
     * Is the data supported by the persister?
     */
    public function supports($data): bool 
    {
        return $data instanceof User;
    }

    /**
     * Persists the data.
     *
     * @param User $data
     */
    public function persist($data) 
    {
        if($data->getPassword()){
            $data->setPassword(
                $this->encoder->encodePassword($data, $data->getPassword())
            );
            $data->setRoles(["ROLE_USER"]);
        }

        $this->em->persist($data);
        $this->em->flush();
    }

    /**
     * Removes the data.
     */
    public function remove($data)
    {
        $this->em->remove($data);
        $this->em->flush();
    }
}
