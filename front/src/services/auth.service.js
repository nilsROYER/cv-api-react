import axios from "axios";


const register = (username, password, nom, prenom) => {
    return axios.post("http://127.0.0.1:8000/api/users", {
        email: username,
        password: password,
        nom: nom, 
        prenom: prenom,
    })
}

const login = (username, password) => {
    return axios.post("http://127.0.0.1:8000/login", {
        email: username,
        password: password
    }).then((response) => {
        if(response.data.token){
            localStorage.setItem("user", JSON.stringify(response.data))
        }
        return response.data
    })
}

const logout = () => {
    localStorage.removeItem("user")
}

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"))
}

const refreshToken = (token, user) => {
    return axios.post("http://127.0.0.1:8000/api/token/refresh", {
        refresh_token: token
    }).then(
        (response) => {
        if(response.data.token) {
            user.token = response.data.token
            user.refresh_token = response.data.refresh_token
            localStorage.setItem("user", JSON.stringify(user))
        }
        return response.data
        }
    )
}

export default { register, login, logout, getCurrentUser, refreshToken }