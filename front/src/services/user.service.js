import axios from "axios";
import authHeader from "./auth.header";
import authService from "./auth.service";

const API_ENTRY_POINT = "http://127.0.0.1:8000/api/users/"

const getPublicContent = () => {
    return axios.get(API_ENTRY_POINT)
}

const getUserBoard = () => {
    return axios.get(API_ENTRY_POINT, {headers: authHeader()})
}

const getModeratorBoard = () => {
    return axios.get(API_ENTRY_POINT + authService.getCurrentUser().id, {headers: authHeader()})
}

const getAdminBoard = () => {
    return axios.get(API_ENTRY_POINT + authService.getCurrentUser().id, {headers: authHeader()})
}

// requétes pour allez chercher tout les infos de l'utilisateur.
const getUserInfos = () => { 
    return axios.get(API_ENTRY_POINT + authService.getCurrentUser().id, {headers: authHeader()})
}



// CRUD REQUEST

// POST experience
const postNewExperience = (poste, description, employeur, adresse, userId) => {
    return axios.post("http://127.0.0.1:8000/api/experiences", {
        poste: poste,
        description: description,
        employeur: employeur,
        adresse: adresse,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// PUT experience
const putExperience = (id, poste, description, employeur, adresse, userId) => {
    return axios.put(`http://127.0.0.1:8000/api/experiences/${id}`, {
        poste: poste,
        description: description,
        employeur: employeur,
        adresse: adresse,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// DELETE experience
const deleteExperience = (id) => {
    return axios.delete(`http://127.0.0.1:8000/api/experiences/${id}`, { headers: authHeader() })
}

// POST competence
const postNewCompetence = (nom, description, userId) => {
    return axios.post("http://127.0.0.1:8000/api/competences", {
        nom: nom ,
        description: description,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// PUT competence
const putCompetence = (id, nom, description, userId) => {
    return axios.put(`http://127.0.0.1:8000/api/competences/${id}`, {
        nom: nom,
        description: description,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// DELETE competence
const deleteCompetence = (id) => {
    return axios.delete(`http://127.0.0.1:8000/api/competences/${id}`, { headers: authHeader() })
}

// POST formations
const postNewFormation = (titre, etablissement, userId) =>{
    return axios.post("http://127.0.0.1:8000/api/formations", {
        titre: titre,
        etablissement: etablissement,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// PUT formation
const putFormation = (id, titre, etablissement, userId) => {
    return axios.put(`http://127.0.0.1:8000/api/formations/${id}`, {
        titre: titre,
        etablissement: etablissement,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// DELETE formation
const deleteFormation = (id) => {
    return axios.delete(`http://127.0.0.1:8000/api/formations/${id}`, { headers: authHeader() })
}

// POST passion
const postNewHobie = (hobie, userId) =>{
    return axios.post("http://127.0.0.1:8000/api/passions", {
        passion: hobie,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// PUT hobie
const putHobie = (id, hobie, userId) => {
    return axios.put(`http://127.0.0.1:8000/api/passions/${id}`, {
        passion: hobie,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// DELETE passion
const deleteHobie = (id) => {
    return axios.delete(`http://127.0.0.1:8000/api/passions/${id}`, { headers: authHeader() })
}

// POST permi
const postNewPermis = (nom, level, vehicule, userId) => {
    return axios.post("http://127.0.0.1:8000/api/permis", {
        nom: nom,
        level: level, 
        vehicule: vehicule,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// PUT permi
const putPermi = (id, nom, level, vehicule, userId) => {
    return axios.put(`http://127.0.0.1:8000/api/permis/${id}`, {
        nom: nom,
        level: level,
        vehicule: vehicule,
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// DELETE permi
const deletePermi = (id) => {
    return axios.delete(`http://127.0.0.1:8000/api/permis/${id}`, { headers: authHeader() })
}

// POST langue
const postNewLangue = (langue, level, userId) =>{
    return axios.post("http://127.0.0.1:8000/api/langues", {
        langue: langue,
        level: parseInt(level), 
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// PUT langue
const putLangue = (id, langue, level, userId) => {
    return axios.put(`http://127.0.0.1:8000/api/langues/${id}`, {
        langue: langue,
        level: parseInt(level),
        user: `/api/users/${userId}`,
        headers: authHeader()
    })
}

// DELETE langue
const deleteLangue = (id) => {
    return axios.delete(`http://127.0.0.1:8000/api/langues/${id}`, { headers: authHeader() })
}


// POST curriculum
const postNewCv = (userId, titre) => {
    return axios.post("http://127.0.0.1:8000/api/curricula", { 
        titre: titre,   
        user: `/api/users/${userId}`,
        headers: authHeader() 
    })
}   

// PUT curriculum
const putCurriculum = (userId, object) => {
    // console.log(object)
    return axios.put(`http://127.0.0.1:8000/api/curricula/${object.id}`, { 
        titre: object.titre,
        experiences: object.experiences,
        formation: object.formations,
        passion: object.passions,
        competences: object.competences,
        langues: object.langues,
        permis: object.permis,
        user: `/api/users/${userId}`
    })
}

export default { 
    getPublicContent,
    getUserBoard,
    getModeratorBoard,
    getAdminBoard,
    getUserInfos,
    postNewExperience,
    postNewCompetence,
    postNewFormation,
    postNewHobie,
    postNewLangue,
    postNewPermis,
    postNewCv,
    deleteCompetence,
    deleteExperience,
    deleteFormation,
    deleteHobie,
    deleteLangue,
    deletePermi,
    putCompetence,
    putExperience,
    putFormation,
    putHobie,
    putLangue,
    putPermi,
    putCurriculum
}