import React, { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import AuthService from "../../services/auth.service";

// verif champ obligatoire
const required = value => {
    if(!value) {
        return <div className="alert alert-danger" role="alert">
            this field is required!
        </div>
    }
}  

// vérif email 2@2.2
const email = value => {
    if (!isEmail(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          This is not a valid email.
        </div>
      );
    }
}

const Login = (props) => {

    console.log(props)
    const form = useRef()
    const checkbtn = useRef()

    // string
    const [username, setUsername] = useState("")    
    // string
    const [password, setPassword] = useState("")
    // bool
    const [loading, setLoading] = useState(false)
    // string
    const [message, setMessage] = useState("")

    const onChangeUsername = (e) =>{
        const username = e.target.value
        setUsername(username)
    }

    const onChangePassword = (e) =>{
        const password = e.target.value
        setPassword(password)
    }

    const handleLogin = (e) => {
        e.preventDefault()

        setMessage("")
        setLoading(true)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0) {
            AuthService.login(username, password).then(
                () => {
                    props.history.push("/profile")
                    window.location.reload()
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setLoading(false)
                    setMessage(resMessage)
                }
            )
        } else {
            setLoading(false)
        }
    }


    return (
        <div className="col-md-12">
            <div className="card card-container">
                <Form onSubmit={handleLogin} ref={form}>
                    <div className="form-group">
                        <label htmlFor="username">Email</label>
                        <Input type="text" name="username" className="form-controle" 
                        value={username} 
                        onChange={onChangeUsername} 
                        validations={[required, email]} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <Input type="password" name="password" className="form-controle" 
                        value={password} 
                        onChange={onChangePassword} 
                        validations={[required]} />
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary btn-block" disabled={loading}>
                            {loading && (
                                <span className="spinner-border spinner-border-sm"></span>
                            )}
                            <span>login</span>
                        </button>
                    </div>

                    {message && (
                        <div className="form-group">
                            <div className="alert alert-danger" role="alert">
                                {message}
                            </div>
                        </div>
                    )}

                    <CheckButton style={{display: "none"}} ref={checkbtn} />
                </Form>
            </div>
        </div>
    )
}



export default Login
