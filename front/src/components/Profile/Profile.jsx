import React from "react"
import AuthService from "../../services/auth.service"

const Profile = () => {
    const currentUser = AuthService.getCurrentUser()
    const userRoles = currentUser.roles

    // const [allcv, setAllcv] = useState(currentUser.user.curriculums)

    return (
        <div>
            <div className="container">
                    <header className="jumbotron">
                    <h3>
                        <strong>{currentUser.email}</strong> 
                    </h3>
                    </header>

                    <p>
                    <strong>Token:</strong> {currentUser.token.substring(0, 20)} ...{" "}
                    {currentUser.token.substr(currentUser.token.length - 20)}
                    </p>

                    <p>
                    <strong>Id:</strong> {currentUser.id}
                    </p>

                    <p>
                    <strong>Email:</strong> {currentUser.email}
                    </p>

                    <strong>Authorities:</strong>
                    <ul>
                    {userRoles &&
                        userRoles.map((role, index) => <li key={index}>{role}</li>)}

                    {/* {userCompetences ? (userCompetences.map((competence, index) => <li key={index}>{competence.nom} <br/>{competence.description}</li>))} */}
                        
                    </ul>
            </div>
        </div>
    )
}

export default Profile