import React, { useState, useRef, useEffect } from "react"
import Form from "react-validation/build/form"
import Input from "react-validation/build/input"
import CheckButton from "react-validation/build/button"
import Select from "react-select"
import {  } from "validator"
import userService from "../../services/user.service"
import AuthService from "../../services/auth.service"
import dontAuthorized from "../../services/autorizator.service"
import { useHistory } from "react-router-dom"
import makeAnimated from 'react-select/animated';
import "./pageCv.css"

const PageCv = () => {
    const animatedComponents = makeAnimated();
    let history = useHistory() // hook pour facilement controller le navigateur
    const [formCv, setFormCv] = useState(false)// boolean = true => créer un cv vide et inject un formulaire pour lui donner un titre puis le persister en bdd.
    const [titre, setTitre] = useState("") // valeur du champ du titre du cv
    const [loading, setLoading] = useState(false) // variable a true l'or d'un chargement => formulaire, http request etc ...
    const checkbtn = useRef()
    const user = AuthService.getCurrentUser() // donnée de l'utilisateur connecter utile au bon fonctionement de l'applications
    const [message, setMessages] = useState("") // variable pour afficher les messages erreur ou success
    const [curriculums, setCurriculums] = useState([]) // rassemble tout les cv de l'utilisateur
    const [err, setErr] = useState(null)

    const [infosMod, setInfos] = useState(false) // si true => la page ce modifie et permet d'ajouter des informations a 1 cv
    const [currentCv, setCurrentCv] = useState(null)
    const [competences, setCompetences] = useState([]) // compétences de l'utilisateur
    const [formations, setFormations] = useState([]) // formations de l'utilisateur
    const [experiences, setExperiences] = useState([]) // experiences de l'utilisateur
    const [langues, setLangues] = useState([]) // langues de l'utilisateur
    const [permis, setPermis] = useState([]) // permis de l'utilisateur
    const [hobies, setHobies] = useState([]) // centres d'intéret de l'utilisateur

    const [selectedCompetence, setSelectedCompetences] = useState("") // valeur du champ select pour les compétences
    const [selectedExperiences, setSelectedExperiences] = useState("") // valeur du champ select pour les expériences
    const [selectedFormations, setSelectedFormations] = useState("") // valeur du champ select pour les formations
    const [selectedHobies, setSelectedHobies] = useState("") // valeur du champ select pour les centres d'intéret
    const [selectedLangues, setSelectedLangues] = useState("") // valeur du champ select pour les langues
    const [selectedPermis, setSelectedPermis] = useState("") // valeur du champ select pour les permis

    const form = useRef()

    useEffect(() => {
        if(!user){
            dontAuthorized(history, "/");
        }   
        setLoading(true)
        userService.getUserInfos().then(
            (res) => {
                console.log(res)
                setCurriculums(res.data.curriculums)
                setCompetences(res.data.competences)
                setFormations(res.data.formations)
                setExperiences(res.data.experiences)
                setLangues(res.data.langues)
                setPermis(res.data.permis)
                setHobies(res.data.hobies)
                setLoading(false)
            },
            (error) => {
                if(error.response.status === 401){
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/compétences")
                        }
                    )
                    setLoading(false)
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                    console.log(err)
                }
            }
        )
    }, [])


    const options = {
        competences: [],
        experiences: [],
        formations: [],
        hobies: [],
        langues: [],
        permis: []
    }

    for(let c = 0; c < competences.length; c++){
        options.competences.push({value: competences[c].id, label: competences[c].id, object: competences[c]})
    }

    for(let e = 0; e < experiences.length; e++){
        options.experiences.push({value: experiences[e].id, label: experiences[e].id, object: experiences[e]})
    }

    for(let f = 0; f < formations.length; f++){
        options.formations.push({value: formations[f].id, label: formations[f].id, object: formations[f]})
    }

    for(let h = 0; h < hobies.length; h++){
        options.hobies.push({value: hobies[h].id, label: hobies[h].id, object: hobies[h]})
    }

    for(let l = 0; l < langues.length; l++){
        options.langues.push({value: langues[l].id, label: langues[l].id, object: langues[l]})
    }

    for(let p = 0; p < permis.length; p++){
        options.permis.push({value: permis[p].id, label: permis[p].id, object: permis[p]})
    }

    const createCv = (e) => {
        e.preventDefault()
        setFormCv(!formCv)
    }

    const onChangeTitle = (e) => {
        const titre = e.target.value
        setTitre(titre)
    }

    const annuleCruds = () => {
        setCurrentCv(null)
        setInfos(false)
        setFormCv(false)
    }

    const onChangeSelectCompetences = selectedOptions => setSelectedCompetences(selectedOptions);
    const onChangeSelectExperiences = selectedOptions => setSelectedExperiences(selectedOptions);
    const onChangeSelectFormations = selectedOptions => setSelectedFormations(selectedOptions);
    const onChangeSelectHobies = selectedOptions => setSelectedHobies(selectedOptions);
    const onChangeSelectLangues = selectedOptions => setSelectedLangues(selectedOptions);
    const onChangeSelectPermis = selectedOptions => setSelectedPermis(selectedOptions);

    const postNewCv = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)

        form.current.validateAll()

        if(0 === checkbtn.current.context._errors.length){

            userService.postNewCv(user.id, titre).then(
                (res) => {
                    console.log(res)
                    setCurriculums([...curriculums, res.data])
                    setCurrentCv(res.data)
                    setTitre("")
                    setSelectedExperiences([])
                    setSelectedCompetences([])
                    setSelectedFormations([])
                    setSelectedHobies([])
                    setSelectedLangues([])
                    setSelectedPermis([])
                    setFormCv(false)
                    setInfos(true)
                    setLoading(false)
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
        }
    }
    
    const onSubmitAttribues = (e) => {
        e.preventDefault()
        setLoading(true)
        const value = []
        const valueExperiences = []
        const valueFormations = []
        const valueHobies = []
        const valueLangues = []
        const valuePermis = []
        const newCV = currentCv
        // console.log(newCV)
        if(selectedCompetence !== null) {
            for (let c = 0; c < selectedCompetence.length; c++) {
                value.push(`/api/competences/${selectedCompetence[c].object.id}`)
            }
        }
        newCV.competences = value

        if (selectedExperiences !== null) {
            for (let e = 0; e < selectedExperiences.length; e++) {
                valueExperiences.push(`/api/experiences/${selectedExperiences[e].object.id}`)
            }
        }
        newCV.experiences = valueExperiences

        if (selectedFormations !== null) {
            for (let e = 0; e < selectedFormations.length; e++) {
                valueFormations.push(`/api/formations/${selectedFormations[e].object.id}`)
            }
        }
        newCV.formation = valueFormations

        if (selectedHobies !== null) {
            for (let e = 0; e < selectedHobies.length; e++) {
                valueHobies.push(`/api/passions/${selectedHobies[e].object.id}`)
            }
        }
        newCV.passion = valueHobies
        
        if (selectedLangues !== null) {
            for (let e = 0; e < selectedLangues.length; e++) {
                valueLangues.push(`/api/langues/${selectedLangues[e].object.id}`)
            }
        }
        newCV.langues = valueLangues

        if (selectedPermis !== null) {
            for (let e = 0; e < selectedPermis.length; e++) {
                valuePermis.push(`/api/permis/${selectedPermis[e].object.id}`)
            }
        }
        newCV.permis = valuePermis
    
        setCurrentCv(newCV)

        userService.putCurriculum(user.id, currentCv).then(
            (res) => {
                // console.log(res)
                setLoading(false)
                annuleCruds()
            },
            (error) => {
                // console.log(error)
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
                setLoading(false)
            }
        )
    }

    const modifCv = (curriculum) => {
        console.log(curriculum)
        setCurrentCv(curriculum)
        const defaultCompetences = []
        const defaultexperiences = []
        const defaultFormations = []
        const defaultHobies = []
        const defaultLangues = []
        const defaultPermis = []

        for(let i = 0; i < curriculum.competences.length; i++) {
            defaultCompetences.push({value: curriculum.competences[i].id, label: curriculum.competences[i].id, object: curriculum.competences[i]})
            // for(let d = 0; d < options.competences.length; d++)
            //     if(curriculum.competences[i] === options.competences[d])
            //         defaultCompetences.push(options.competences)
            //     }
        }
        for(let i = 0; i < curriculum.experiences.length; i++) {
            defaultexperiences.push({value: curriculum.experiences[i].id, label: curriculum.experiences[i].id, object: curriculum.experiences[i]})
        }
        for(let i = 0; i < curriculum.formation.length; i++) {
            defaultFormations.push({value: curriculum.formation[i].id, label: curriculum.formation[i].id, object: curriculum.formation[i]})
        }
        for(let i = 0; i < curriculum.passion.length; i++) {
            defaultHobies.push({value: curriculum.passion[i].id, label: curriculum.passion[i].id, object: curriculum.passion[i]})
        }
        for(let i = 0; i < curriculum.langues.length; i++) {
            defaultLangues.push({value: curriculum.langues[i].id, label: curriculum.langues[i].id, object: curriculum.langues[i]})
        }
        for(let i = 0; i < curriculum.permis.length; i++) {
            defaultPermis.push({value: curriculum.permis[i].id, label: curriculum.permis[i].id, object: curriculum.permis[i]})
        }

        setSelectedCompetences(defaultCompetences)
        setSelectedExperiences(defaultexperiences)
        setSelectedFormations(defaultFormations)
        setSelectedLangues(defaultLangues)
        setSelectedHobies(defaultHobies)
        setSelectedPermis(defaultPermis)
        setFormCv(false)
        setInfos(true)
    }

    if(loading) {
        return "chargement..."
    }

    return (
        <div>
            <h2>Mes cv</h2>
            <ul>
            {curriculums.map(curriculum => <li key={curriculum.id}>{curriculum.titre} <button onClick={() =>modifCv(curriculum)} className="btn btn-primary">Enregistrer</button></li>)}
            </ul>
            <button onClick={createCv} className="btn btn-success">Ajouter un cv</button>


            {formCv && (
                <div className="col-md-12">
                    <div className="card card-container">
                        <Form onSubmit={postNewCv} ref={form}>
                            <div className="form-group">
                                <label htmlFor="titre">Titre de votre CV</label>
                                <Input name="titre" type="text" className="form-control"
                                value={titre}
                                onChange={onChangeTitle}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                            <button className="btn btn-primary btn-block" disabled={loading}>
                                    {loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Enregistrer</span>
                                </button>
                            </div>
                            <CheckButton style={{display: "none"}} ref={checkbtn} />
                        </Form>
                    </div>
                </div>
            )}

            {infosMod && (
                <div>
                    <Form onSubmit={onSubmitAttribues} ref={form}>
                        <div className="form-group">
                            <label htmlFor="selectCompetences">Competences</label>
                            <Select
                            name="selectCompetences"
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti
                            value={selectedCompetence}
                            onChange={onChangeSelectCompetences}
                            options={options.competences}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="selectExperiences">Experiences</label>
                            <Select
                            name="selectExperiences"
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti
                            value={selectedExperiences}
                            onChange={onChangeSelectExperiences}
                            options={options.experiences}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="selectFormations">Formations</label>
                            <Select
                            name="selectFormations"
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti
                            value={selectedFormations}
                            onChange={onChangeSelectFormations}
                            options={options.formations}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="selectHobies">Hobies</label>
                            <Select
                            name="selectHobies"
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti
                            value={selectedHobies}
                            onChange={onChangeSelectHobies}
                            options={options.hobies}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="selectLangues">Langues</label>
                            <Select
                            name="selectLangues"
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti
                            value={selectedLangues}
                            onChange={onChangeSelectLangues}
                            options={options.langues}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="selectPermis">Permis</label>
                            <Select
                            name="selectPermis"
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti
                            value={selectedPermis}
                            onChange={onChangeSelectPermis}
                            options={options.permis}
                            />
                        </div>

                        <div className="form-group">
                            <button className="btn btn-primary btn-block" disabled={loading}>
                                {loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                )}
                                <span>Enregistrer</span>
                            </button>
                        </div>
                    </Form>
                </div>
            )}
        </div>
    )
}

export default PageCv