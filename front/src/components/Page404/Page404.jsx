import { render } from "@testing-library/react"
import React from "react"

const Page404 = ({location}) => {
    return (
        <div>
            <h2><strong>404 </strong>No match found for <code>{location.pathname}</code></h2>
        </div>
    )
}

export default Page404;