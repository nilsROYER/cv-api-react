import React, { useEffect, useState } from "react"
import AuthService from "../../services/auth.service"

import UserService from "../../services/user.service"
import dontAuthorized from "../../services/autorizator.service"

const Home = (props) => {
    const [content, setContent] = useState([])
    console.log(content)

    useEffect(() => {
        UserService.getUserBoard().then(
            (response) => {
                setContent(response.data["hydra:member"])
            },
            (error) => {
                if(error.response.status === 401){
                    const user = JSON.parse(localStorage.getItem("user"))
                    if(!user){
                        dontAuthorized(props, "/");
                    }   
                    console.log(user)
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            props.history.push("/home")
                            window.location.reload()
                        }
                    )
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setContent(_content)
                }        
            }
        )
    }, [])

    return (
        <div className="container">
            <header className="jumbotron">
                {content.map(item => <h3 key={item.id}>{item.email}</h3>)}
            </header>
        </div>
    )
}

export default Home