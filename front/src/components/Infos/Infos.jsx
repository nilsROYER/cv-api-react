import React from "react"
import { useEffect ,useState } from "react"
import AuthService from "../../services/auth.service"
import UserService from "../../services/user.service"
import "./infos.css"
import dontAuthorized from "../../services/autorizator.service"

import BlockCompetences from "./blocks/BlockCompetences"
import BlockExperience from "./blocks/BlockExperience"
import BlockFormations from "./blocks/BlockFormations"
import BlockHobies from "./blocks/BlockHobies"
import BlockLangues from "./blocks/BlockLangues"
import BlockPermis from "./blocks/BlockPermis"
import { Link } from "react-router-dom"


const Infos = ({children = null}) => {

    const user = AuthService.getCurrentUser()

    const [err, setErr] = useState(null)


    return (
        <div>
             <nav className="menu">
              <li>
                <Link to={"/infos/compétences"} className="nav-link">
              compétences
                </Link>
              </li>
              <li>
                <Link to={"/infos/expériences"} className="nav-link">
                expériences
                </Link>
              </li>
              <li>
                <Link to={"/infos/formations"} className="nav-link">
                formations
                </Link></li>
              <li>
                <Link to={"/infos/Hobies"} className="nav-link">
                Centres d'intéret
                </Link>
              </li>
              <li>
                <Link to={"/infos/langues"} className="nav-link">
                langues
                </Link>
              </li>
              <li>
                <Link to={"/infos/Permis"} className="nav-link">
                Permis
                </Link>
              </li>
             </nav>
            
            {(children) && (
                <div>{children}</div>
            )}
        </div>
    )
}

export default Infos