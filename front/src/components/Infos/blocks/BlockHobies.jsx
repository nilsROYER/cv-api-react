import React from "react"
import { useEffect, useState, useRef } from "react"
import UserService from "../../../services/user.service"
import AuthService from "../../../services/auth.service"
import dontAuthorized from "../../../services/autorizator.service"
import Form from "react-validation/build/form";
import Textarea from "react-validation/build/textarea";
import CheckButton from "react-validation/build/button";
import { useHistory } from "react-router-dom";

const BlockHobies = ({ title = "Title"}) => {

    const [userHobies, setHobies] = useState([])
    const [err, setErr] = useState(null)
    const form = useRef()
    const checkbtn = useRef()

    // si true affiche un formulaire
    const [newHb, setNewHb] = useState(false)
    // si true block le button
    const [loading, setLoading] = useState(false)
    // messages
    const [message, setMessages] = useState("")
    const user = AuthService.getCurrentUser()
    const [hobie, setPassion] = useState("")

    const [currentId, setCurrentId] = useState(null)
    const [modif, setModif] = useState(false)

    let history = useHistory()

    useEffect(() => {
        
        if(!user){
            dontAuthorized(history, "/");
        }   
        
        setLoading(true)
        UserService.getUserInfos().then(
            (response) => {
                const user = response.data
                setHobies(user.hobies)
                setLoading(false)
            },
            (error) => {
                if(error.response.status === 401){
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/hobies")
                            window.location.reload()
                        }
                    )
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                }
            }
        )
    }, [])

    const injectFormCompetence = (e) => {
        e.preventDefault()
        setPassion("")
        setNewHb(!newHb)
        setCurrentId(null)
        if(modif) setModif(false)
    }

    const onChangeHobie = (e) => {
        const hobie = e.target.value
        setPassion(hobie)
    }

    // traitement du formulaire
    const onSubmitHobie = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.postNewHobie(hobie, user.id).then(
                (response) => {
                    setHobies([...userHobies, response.data])
                    setPassion("")
                    setMessages(response.data.message)
                    setLoading(false)
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                }
            )
        }
    }

    const onDeleteHobie = (id) => {

        UserService.deleteHobie(id).then(
            ()=> {
                const del = userHobies.filter(hobie => id !== hobie.id)
                setHobies(del)
            }, 
            (error) => {
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
            }
            
        )
    }

    const onPutElement = (id, hobie) => {
        setPassion(hobie)
        setCurrentId(id)
        setModif(true)

        if(newHb) setNewHb(false)
    }

    const onModifHobie = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.putHobie(currentId, hobie, user.id).then(
                (res) => {
                    const diff = userHobies.filter(hobie => currentId !== hobie.id)
                    diff.push(res.data)
                    setHobies(diff)
                
                    setMessages(res.statusText)
                    setPassion("")
                    setLoading(false)
                    annuleCruds()
                    setCurrentId(null)
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
            
        }
    }

    const annuleCruds = () => {
        setCurrentId(null)
        setNewHb(false)
        setModif(false)
    }

    const isModif = (id) => {
        if(id === currentId) return true
        else return false
    }

    if(loading) {
        return "chargement..."
    }

    return (
        <div>
            <h1>{title}</h1>
            <ul>{userHobies.map(({id, passion}) => 
                <li key={id}>{passion}
                        <button onClick={() => onDeleteHobie(id)}className="btn btn-danger">supprimer</button>
                        <button onClick={() => onPutElement(id, passion)} className="btn btn-secondary" disabled={isModif(id)}>Modifier</button>
                    </li>
                )}</ul>
            <div>
                <button onClick={injectFormCompetence} className="btn btn-primary">ajouter un nouveau centre d'intéret</button>
                
            </div>
            {(newHb || modif ) && (
                <div className="col-md-12">
                    <div className="card card-container">
                        <Form onSubmit={newHb ? onSubmitHobie : onModifHobie} ref={form}>
                            <div className="form-group">
                                <label htmlFor="hobie">Hobie</label>
                                <Textarea type="text" name="hobie" className="form-control"
                                value={hobie}
                                onChange={onChangeHobie}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary btn-block" disabled={loading}>
                                    {loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Envoyer</span>
                                </button>
                                <button className="btn btn-info btn-block" onClick={annuleCruds}>annuler</button>
                            </div>
                            <CheckButton style={{display: "none"}} ref={checkbtn} />        
                        </Form>
                    </div >
                </div>
            )}
        </div>
    )
}

export default BlockHobies