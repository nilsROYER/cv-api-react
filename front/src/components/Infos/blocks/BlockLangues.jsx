import React from "react"
import { useEffect, useState, useRef } from "react"
import AuthService from "../../../services/auth.service"
import UserService from "../../../services/user.service"
import dontAuthorized from "../../../services/autorizator.service"
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { useHistory } from "react-router-dom";

const BlockLangues = ({title = "Title"}) => {

    const [userLangues, setLanguages] = useState([])
    const [err, setErr] = useState(null)

    const form = useRef()
    const checkbtn = useRef()

    // si true affiche un formulaire
    const [newLgue, setLgue] = useState(false)
    // si true block le button
    const [loading, setLoading] = useState(false)
    // messages
    const [message, setMessages] = useState("")
    const [langue, setLangue] = useState("")
    const [level, setLevel] = useState("")
    const user = AuthService.getCurrentUser()
    const [currentId, setCurrentId] = useState(null)
    const [modif, setModif] = useState(false)
    
    let history = useHistory()

    useEffect(() => {
        
        if(!user){
            dontAuthorized(history, "/");
        }   

        setLoading(true)
        UserService.getUserInfos().then(
            (response) => {
                const user = response.data
                setLanguages(user.langues)
                setLoading(false)
            },
            (error) => {
                
                if(error.response.status === 401){
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/langues")
                            window.location.reload()
                        }
                    )
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                }
            }
        )  
    }, [])

    const injectFormCompetence = (e) => {
        e.preventDefault()
        setLangue("")
        setLevel("0")
        setLgue(!newLgue)
        setCurrentId(null)
        if(modif) setModif(false)
    }

    const onChangeLangue= (e) => {
        const langue = e.target.value
        setLangue(langue)
    }

    const onChangeLevel= (e) => {
        const level = e.target.value
        setLevel(level)
    }

    // traitement du formulaire
    const onSubmitLangue = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.postNewLangue(langue, level, user.id).then(
                (response) => {
                    setLanguages([...userLangues, response.data])
                    setLangue("")
                    setLevel(0)
                    setMessages(response.data.message)
                    setLoading(false)
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                }
            )
        }

        setLoading(false)
    }

    const onDeleteLangue = (id) => {

        UserService.deleteLangue(id).then(
            ()=> {
                const del = userLangues.filter(langue => id !== langue.id)
                setLanguages(del)
            }, 
            (error) => {
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
            }
            
        )
    }

    const onPutElement = (id, langue, level) => {
        setLangue(langue)
        setLevel(level)
        setCurrentId(id)
        setModif(true)

        if(newLgue) setLgue(false)
    }

    const onModifLangue = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.putLangue(currentId, langue, level, user.id).then(
                (res) => {
                    const diff = userLangues.filter(langue => currentId !== langue.id)
                    diff.push(res.data)
                    setLanguages(diff)
                
                    setMessages(res.statusText)
                    setLangue("")
                    setLevel("")
                    setLoading(false)
                    setCurrentId(null)
                    annuleCruds()
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
            
        }
    }

    const annuleCruds = () => {
        setCurrentId(null)
        setLgue(false)
        setModif(false)
    }

    const isModif = (id) => {
        if(id === currentId) return true
        else return false
    }

    if(loading) {
        return "chargement..."
    }

    return (
        <div>
            <h1>{title}</h1>
            <ul>{userLangues.map(({id, langue, level}) => 
                <li key={id}>{langue} {level}
                    <button onClick={() => onDeleteLangue(id)} className="btn btn-primary">supr</button>
                    <button onClick={() => onPutElement(id, langue, level)} className="btn btn-secondary" disabled={isModif(id)}>Modifier</button>
                </li>)}
            </ul>

            <div>
                <button onClick={injectFormCompetence} className="btn btn-primary">ajouter une langue</button>
            </div>

            {(newLgue || modif) && (
                <div className="col-md-12">
                    <div className="card card-container">
                        <Form onSubmit={newLgue ? onSubmitLangue : onModifLangue} ref={form}>
                            <div className="form-group">
                                <label htmlFor="langue">Hobie</label>
                                <Input type="text" name="langue" className="form-control"
                                value={langue}
                                onChange={onChangeLangue}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="level">lvl</label>
                                <Input type="number" name="level" className="form-control"
                                value={level}
                                onChange={onChangeLevel}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary btn-block" disabled={loading}>
                                    {loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Envoyer</span>
                                </button>
                                <button className="btn btn-info btn-block" onClick={annuleCruds}>annuler</button>
                            </div>
                            <CheckButton style={{display: "none"}} ref={checkbtn} />
                        </Form>
                    </div>
                </div>
            )}
        </div>
    )
}

export default BlockLangues