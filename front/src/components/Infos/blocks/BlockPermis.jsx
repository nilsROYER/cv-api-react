import React from "react"
import { useEffect, useState, useRef } from "react"
import AuthService from "../../../services/auth.service"
import UserService from "../../../services/user.service"
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { useHistory } from "react-router-dom";
import dontAuthorized from "../../../services/autorizator.service"

const BlockPermis = ({title = "Title"}) => {

    const [userPermis, setPermis]  = useState([])
    const user = AuthService.getCurrentUser()
    const [err, setErr] = useState(null)

    const form = useRef()
    const checkbtn = useRef()

    // si true affiche un formulaire
    const [newPermi, setPermi] = useState(false)
    // si true block le button
    const [loading, setLoading] = useState(false)
    // messages
    const [message, setMessages] = useState("")
    
    const [nom, setNom] = useState("")
    const [level, setLevel] = useState("")
    const [vehicule, setVehicule] = useState(false)

    const [currentId, setCurrentId] = useState(null)
    const [modif, setModif] = useState(false)
    let history = useHistory()

    useEffect(() => {
        
        if(!user){
            dontAuthorized(history, "/");
        }   
        
        setLoading(true)
        UserService.getUserInfos().then(
            (response) => {
                const user = response.data
                setPermis(user.permis)
                setLoading(false)
            },
            (error) => {
                if(error.response.status === 401){
                  
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/permis")
                            window.location.reload()
                        }
                    )
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                }
            }
        )
    }, [])

    const injectFormCompetence = (e) => {
        e.preventDefault()
        setNom("")
        setLevel("")
        setVehicule(false)
        setCurrentId(null)
        setPermi(!newPermi)
        if(modif) setModif(false)
    }

    const onChangeNom= (e) => {
        const nom = e.target.value
        setNom(nom)
    }

    const onChangeLevel= (e) => {
        const level = e.target.value
        setLevel(level)
    }

    const onChangeVehicule = (e) => {
        const vehicule = e.target.value
        setVehicule(!vehicule)
    }

    // traitement du formulaire
    const onSubmitPermi = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.postNewPermis(nom, level, vehicule, user.id).then(
                (response) => {
                    setPermis([...userPermis, response.data])
                    setNom("")
                    setLevel("")
                    setVehicule(false)
                    setMessages(response.data.message)
                    setLoading(false)
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                }
            )
        }
    }

    const onDeletePermis = (id) => {

        UserService.deletePermi(id).then(
            ()=> {
                const del = userPermis.filter(permi => id !== permi.id)
                setPermis(del)
            }, 
            (error) => {
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
            }
            
        )
    }

    const onPutElement = (id, nom, level, vehicule) => {
        setNom(nom)
        setLevel(level)
        setVehicule(false)
        setCurrentId(id)
        setModif(true)

        if(newPermi) setPermi(false)
    }

    const onModifPermi = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.putPermi(currentId, nom, level, vehicule, user.id).then(
                (res) => {
                    const diff = userPermis.filter(permi => currentId !== permi.id)
                    diff.push(res.data)
                    setPermis(diff)
                
                    setMessages(res.statusText)
                    setNom("")
                    setLevel("")
                    setVehicule(false)
                    setLoading(false)
                    setCurrentId(null)
                    annuleCruds()
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
            
        }
    }

    const annuleCruds = () => {
        setCurrentId(null)
        setPermi(false)
        setModif(false)
    }

    const isModif = (id) => {
        if(id === currentId) return true
        else return false
    }

    if(loading){
        return "chargement..."
    }
    
    return (
        <div>
            <h1>{title}</h1>
            <ul>{userPermis.map(({id, nom, level, vehicule}) => (
                <li key={id}>{nom} 
                    <button onClick={() => onDeletePermis(id)} className="btn btn-primary">supr</button>
                    <button onClick={() => onPutElement(id, nom, level, vehicule)} className="btn btn-secondary" disabled={isModif(id)}>Modifier</button>
                </li> ))}
            </ul>
            <div>
                <button onClick={injectFormCompetence} className="btn btn-primary">ajouter une compétence</button>
            </div>
            {(newPermi || modif) && (
                <div className="col-md-12">
                    <div className="card card-container">
                        <Form onSubmit={newPermi ? onSubmitPermi : onModifPermi} ref={form}>
                            <div className="form-group">
                                <label htmlFor="nom">Hobie</label>
                                <Input type="text" name="nom" className="form-control"
                                value={nom}
                                onChange={onChangeNom}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="level">lvl</label>
                                <Input type="text" name="level" className="form-control"
                                value={level}
                                onChange={onChangeLevel}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="vehicule">vehicule</label>
                                <Input type="checkbox" name="vehicule" className="form-control"
                                value={vehicule}
                                onChange={onChangeVehicule}
                                validations={[]}
                                />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary btn-block" disabled={loading}>
                                    {loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Envoyer</span>
                                </button>
                                <button className="btn btn-info btn-block" onClick={annuleCruds}>annuler</button>
                            </div>
                            <CheckButton style={{display: "none"}} ref={checkbtn} />
                        </Form>
                    </div>
                </div>
            )}
        </div>
    )
}

export default BlockPermis