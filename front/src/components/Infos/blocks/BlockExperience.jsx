import React, { useRef } from "react"
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import {  } from "validator";
import { useState } from "react";
import UserService from "../../../services/user.service";
import { useEffect } from "react";
import dontAuthorized from "../../../services/autorizator.service"
import AuthService from "../../../services/auth.service"
import { useHistory } from "react-router-dom";

const BlockExperiences = ({ title = "Title"}) => {

    const [userExperience, setExperiences] = useState([])
    const [err, setErr] = useState(null)
    const user = AuthService.getCurrentUser()
    let history = useHistory()

    

    const form = useRef()
    const checkbtn = useRef()

    // si true affiche un formulaire
    const [newExp, setNewExp] = useState(false)
    const [modif, setModif] = useState(false)
    // si true block le button
    const [loading, setLoading] = useState(false)
    // messages
    const [message, setMessages] = useState("")
    

    const [poste, setPoste] = useState("")
    const [description, setDescription] = useState("")
    const [employeur, setEmployeur] = useState("")
    const [adresse, setAdresse] = useState("")

    const [currentId, setCurrentId] = useState(null)

    useEffect(() => {

        if(!user){
            dontAuthorized(history, "/");
        }  

        setLoading(true)
        UserService.getUserInfos().then(
            (response) => {
                const user = response.data
                setExperiences(user.experiences)
                setLoading(false)
            },
            (error) => {
                if(error.response.status === 401){
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/expériences")
                            window.location.reload()
                        }
                    )
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                }
            }
        )
    }, [])

    const injectFormCompetence = (e) => {
        e.preventDefault()
        setModif(false)
        setPoste("")
        setDescription("")
        setEmployeur("")
        setAdresse("")
        setNewExp(!newExp)
        setCurrentId(null)
        if(modif) setModif(false)
    }

    const onChangePoste = (e) => {
        const poste = e.target.value
        setPoste(poste)
    }

    const onChangeDescription = (e) => {
        const description = e.target.value
        setDescription(description)
    }

    const onChangeAdresse = (e) => {
            const adresse = e.target.value
            setAdresse(adresse)
    }

    const onChangeEmployeur = (e) => {
        const employeur = e.target.value
        setEmployeur(employeur)
    }

    

    // traitement du formulaire
    const onSubmitExperience = (e) => {
        e.preventDefault()
        setLoading(true)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.postNewExperience(poste, description, employeur, adresse, user.id).then(
                (response) => {
                    setExperiences([...userExperience, response.data])
                    setPoste("")
                    setDescription("")
                    setEmployeur("")
                    setAdresse("")
                    
                    setMessages(response.statusText)
                    setLoading(false)
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                }
            )
        }
    }

    const onDeleteExperience = (id) => {
            
        UserService.deleteExperience(id).then(
            () => {
                const del = userExperience.filter(exp => id !== exp.id)
                setExperiences(del)
            },
            (error) => {
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
            }
        )
    }

    const onPutElement = (id, poste, description, employeur, adresse) => {
        setPoste(poste)
        setDescription(description)
        setEmployeur(employeur)
        setAdresse(adresse)
        setCurrentId(id)
        setModif(true)

        if(newExp) setNewExp(false)
    }

    const onModifExperience = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.putExperience(currentId, poste, description, employeur, adresse, user.id).then(
                (res) => {
                    const diff = userExperience.filter(experience => currentId !== experience.id)
                    diff.push(res.data)
                    setExperiences(diff)
                
                    setMessages(res.statusText)
                    setPoste("")
                    setEmployeur("")
                    setAdresse("")
                    setDescription("")
                    setLoading(false)
                    annuleCruds()
                    setCurrentId(null)
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
            
        }
    }

    const annuleCruds = () => {
        setCurrentId(null)
        setNewExp(false)
        setModif(false)
    }

    const isModif = (id) => {
        if(id === currentId) return true
        else return false
    }

    if(loading) {
        return "chargement..."
    }   

    return (
        <div>
            <h1>{title}</h1>
            <ul>{userExperience.map(({id, description, poste, employeur, adresse}) => <li key={id}>{description} <button className="btn btn-danger" 
                onClick={() => onDeleteExperience(id)}>supprimer</button>
                <button onClick={() => onPutElement(id, poste, description, employeur, adresse)} className="btn btn-secondary" disabled={isModif(id)}>Modifier</button></li>)}</ul>
            <div>
                <button onClick={injectFormCompetence} className="btn btn-primary">Ajouter une experience</button>
            </div>

        {(newExp || modif) && (
            <div className="col-md-12">
                <div className="card card-container">
                    <Form onSubmit={newExp ? onSubmitExperience : onModifExperience} ref={form}>
                        <div className="form-group">
                            <label htmlFor="poste">Poste</label>
                            <Input type="text" name="poste" className="form-control"
                            value={poste}
                            onChange={onChangePoste}
                            validations={[]}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <Input type="text" name="description" className="form-control"
                            value={description}
                            onChange={onChangeDescription}
                            validations={[]}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="employeur">employeur</label>
                            <Input type="text" name="employeur" className="form-control"
                            value={employeur}
                            onChange={onChangeEmployeur}
                            validations={[]}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="adresse">Adresse</label>
                            <Input type="text" name="adresse" className="form-control"
                            value={adresse}
                            onChange={onChangeAdresse}
                            validations={[]}
                            />
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary btn-block" disabled={loading}>
                                {loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                )}
                                <span>Envoyer</span>
                            </button>
                            <button className="btn btn-info btn-block" onClick={annuleCruds}>annuler</button>
                        </div>
                    </Form>
                </div>
            </div>
        )}           
        </div>
    )
}

export default BlockExperiences