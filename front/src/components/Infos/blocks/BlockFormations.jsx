import React from "react"
import { useEffect, useState, useRef } from "react"
import dontAuthorized from "../../../services/autorizator.service"
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../../../services/auth.service"
import UserService from "../../../services/user.service"
import { useHistory } from "react-router-dom";

const BlockFormations = ({title = "Title"}) => {

    const [userFormations, setFormations] = useState([])
    const [err, setErr] = useState(null)
    const user = AuthService.getCurrentUser()

    const form = useRef()
    const checkbtn = useRef()
    // si true affiche un formulaire
    const [newFmt, setNewFmt] = useState(false)
    // si true block le button
    const [loading, setLoading] = useState(false)
    const [message, setMessages] = useState("")

    const [titre, setTitre] = useState("")
    const [etablissement, setEtablissement] = useState("")
    let history = useHistory()


    const [currentId, setCurrentId] = useState(null)
    const [modif, setModif] = useState(false)

    useEffect(() => {
        
        if(!user){
            dontAuthorized(history, "/");
        }  
        
        setLoading(true)
        UserService.getUserInfos().then(
            (response) => {
                const user = response.data
                setFormations(user.formations)
                setLoading(false)
            },
            (error) => {
                if(error.response.status === 401){
                    const user = JSON.parse(localStorage.getItem("user"))
                    
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/formations")
                            window.location.reload()
                        }
                    )
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                }
            }
        )
    }, [])

    const injectFormCompetence = (e) => {
        e.preventDefault()
        setTitre("")
        setEtablissement("")
        setNewFmt(!newFmt)
        setCurrentId(null)
        if(modif) setModif(false)
    }

    const onChangeTitre = (e) => {
        const titre = e.target.value
        setTitre(titre)
    }

    const onChangeEtablissement = (e) => {
        const etablissement = e.target.value
        setEtablissement(etablissement)
    }

    const onSubmitFormation = (e) => {
        e.preventDefault()

     
        setLoading(true)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.postNewFormation(titre, etablissement, user.id).then(
                (response) => {

                    setFormations([...userFormations, response.data])
                    setTitre("")
                    setEtablissement("")
                    setMessages(response.data.message)
                    setLoading(false )
                    
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                }
            )
        }
        // form.current.validateAll()
        // TODO : envoyer en POST pour ajouter une nouvelle compétence.
    }

    const onDeleteFormation = (id) => {

        UserService.deleteFormation(id).then(
            ()=> {
                const del = userFormations.filter(forma => id !== forma.id)
                setFormations(del)
            }, 
            (error) => {
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
            }
            
        )
    }

    const onPutElement = (id, titre, etablissement) => {
        setTitre(titre)
        setEtablissement(etablissement)
        setCurrentId(id)
        setModif(true)

        if(newFmt) setNewFmt(false)
    }

    const onModifFormation = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.putFormation(currentId, titre, etablissement, user.id).then(
                (res) => {
                    const diff = userFormations.filter(formation => currentId !== formation.id)
                    diff.push(res.data)
                    setFormations(diff)
                
                    setMessages(res.statusText)
                    setTitre("")
                    setEtablissement("")
                    setLoading(false)
                    annuleCruds()
                    setCurrentId(null)
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
            
        }
    }

    const annuleCruds = () => {
        setCurrentId(null)
        setNewFmt(false)
        setModif(false)
    }

    const isModif = (id) => {
        if(id === currentId) return true
        else return false
    }

    if(loading) {
        return "chargement..."
    }

    return (
        <div>
            <h1>{title}</h1>
            <ul>{userFormations.map(({id, titre, etablissement}) => 
                <li key={id}>{titre} 
                    <button onClick={() => onDeleteFormation(id)} className="btn btn-danger">supprimer</button>
                    <button onClick={() => onPutElement(id, titre, etablissement)} className="btn btn-secondary" disabled={isModif(id)}>Modifier</button>
                </li>)}
            </ul>
            <div>
                <button onClick={injectFormCompetence} className="btn btn-primary">Ajouter une Formation</button>
            </div>

            {(newFmt || modif) && (
            <div className="col-md-12">
                <div className="card card-container">
                    <Form onSubmit={newFmt ? onSubmitFormation : onModifFormation } ref={form}>
                    <div className="form-group">
                        <label htmlFor="titre">titre</label>
                        <Input type="text" name="titre" className="form-control"
                        value={titre}
                        onChange={onChangeTitre}
                        validations={[]}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="etablissement">etablissement</label>
                        <Input type="text" name="etablissement" className="form-control"
                        value={etablissement}
                        onChange={onChangeEtablissement}
                        validations={[]}
                        />
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary btn-block" disabled={loading}>
                            {loading && (
                                <span className="spinner-border spinner-border-sm"></span>
                            )}
                            <span>Envoyer</span>
                        </button>
                        <button className="btn btn-info btn-block" onClick={annuleCruds}>annuler</button>
                    </div>
                    
                    
                    <CheckButton style={{display: "none"}} ref={checkbtn} />
                    </Form>
                </div>
            </div>
            )}
            
        </div>
    )
}

export default BlockFormations