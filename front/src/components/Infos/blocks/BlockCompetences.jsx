import React from "react"
import { useEffect, useState, useRef } from "react"
import dontAuthorized from "../../../services/autorizator.service"
import AuthService from "../../../services/auth.service"
import UserService from "../../../services/user.service"
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Textarea from "react-validation/build/textarea"
import CheckButton from "react-validation/build/button";
import {  } from "validator";
import { useHistory } from "react-router-dom";

const BlockCompetences = ({ title = "Title"}) => {

    const [userCompetences, setCompetences] = useState([])
    const [err, setErr] = useState(null)
    const user = AuthService.getCurrentUser()
    const form = useRef()
    const checkbtn = useRef()

    let history = useHistory()

    
    const [nom, setNom] = useState("")
    const [description, setDescription] = useState("")
    // si true affiche un formulaire
    const [newCpt, setNewCpt] = useState(false)
    const [modif, setModif] = useState(false)
    // si true block le button
    const [loading, setLoading] = useState(false)
    const [message, setMessages] = useState("")
    const [currentId, setCurrentId] = useState(null)

    useEffect(() => {

        if(!user){
            dontAuthorized(history, "/");
        }   

        setLoading(true)
        UserService.getUserInfos().then(
            (response) => {
                const user = response.data
                setCompetences(user.competences)
                setLoading(false)
            },
            (error) => {
                if(error.response.status === 401){
                    const refresh = user.refresh_token
                    AuthService.refreshToken(refresh, user).then(
                        () => {
                            history.push("/infos/compétences")
                        }
                    )
                    setLoading(false)
                }else {
                    const _content = (error.response && error.response.data) ||
                    error.message ||
                    error.toString();

                    setErr(_content)
                    setLoading(false)
                    console.log(err)
                }
            }
        )
        
    }, [])

    const injectFormCompetence = (e) => {
        e.preventDefault()
        setNom("")
        setDescription("")
        setNewCpt(!newCpt)
        setCurrentId(null)
        if(modif) setModif(false)
    }

    const onChangeNom = (e) => {
        const nom = e.target.value
        setNom(nom)
    }

    const onChangeDescription = (e) => {
        const description = e.target.value
        setDescription(description)
    }

    const onSubmitCompetence = (e) => {
        e.preventDefault()
        
        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.postNewCompetence(nom, description, user.id).then(
                (response) => {
                    setCompetences([...userCompetences, response.data])
                    setMessages(response.statusText)
                    setNom("")
                    setDescription("")
                    setLoading(false)   
                }, 
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                }

            )
        }

        // form.current.validateAll()
        // TODO : envoyer en POST pour ajouter une nouvelle compétence.
    }

    const onDeleteCompetence = (id) => {
        
        UserService.deleteCompetence(id).then(
            () => {
                const del = userCompetences.filter(competence => id !== competence.id)
                setCompetences(del)
            },
            (error) => {
                const resMessage = (error.response && 
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();

                setMessages(resMessage)
            }
        )
    }

    
    const onPutElement = (id, nom, description) => {

        setNom(nom)
        setDescription(description)
        setCurrentId(id)
        setModif(true)
        if(newCpt) setNewCpt(false)
    }

    const onModifCompetence = (e) => {
        e.preventDefault()

        setMessages("")
        setLoading(true)    

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0){
            UserService.putCompetence(currentId, nom, description, user.id).then(
                (res) => {
                    const diff = userCompetences.filter(competence => currentId !== competence.id)
                    diff.push(res.data)
                    setCompetences(diff)
                
                    setMessages(res.statusText)
                    setNom("")
                    setDescription("")
                    setLoading(false)
                    annuleCruds()
                },
                (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessages(resMessage)
                    setLoading(false)
                    console.log(message)
                }
            )
            
        }
    }

    const annuleCruds = () => {
        setCurrentId(null)
        setNewCpt(false)
        setModif(false)
    }

    const isModif = (id) => {
        if(id === currentId) return true
        else return false
    }

    if(loading) return "chargement..."

    return (
        <div>
            <h1>{title}</h1>
            <ul>
                {userCompetences.map(({id, nom, description}) => (<li key={id}>{nom}<button key={id} onClick={() => onDeleteCompetence(id)}
                className="btn btn-danger">Supprimer n {id}</button>
                <button onClick={() => onPutElement(id, nom, description)} className="btn btn-secondary" disabled={isModif(id)}>Modifier</button></li>))}
            </ul>   
            <div>
                <button onClick={injectFormCompetence} className="btn btn-primary">Ajouter une compétence</button>
            </div>
            {(newCpt || modif) && (
                <div className="col-md-12">
                    <div className="card card-container">
                        <Form onSubmit={newCpt ? onSubmitCompetence : onModifCompetence} ref={form}>
                            <div className="form-group">
                                <label htmlFor="nom">Nom</label>
                                <Input type="text" name="nom" className="form-control"
                                value={nom}
                                onChange={onChangeNom}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">Description</label>
                                <Textarea type="text" name="description" className="form-control"
                                value={description}
                                onChange={onChangeDescription}
                                />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary btn-block" disabled={loading}>
                                    {loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Envoyer</span>
                                </button>
                                <button className="btn btn-info btn-block" onClick={annuleCruds}>annuler</button>
                            </div>

                            {/* {message && (
                        <div className="form-group">
                            <div className={!successFull ? "alert alert-danger": "alert alert-success"} role="alert">
                                {message}
                            </div>
                        </div>
                    )} */}
                            
                            <CheckButton style={{display: "none"}} ref={checkbtn} />    
                        </Form>
                    </div>
                </div>
            )}
        </div>
        )

}

export default BlockCompetences