import React, { useRef, useState } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import AuthService from "../../services/auth.service";

// method input validation
const required = (value) => {
    if(!value) {
        return <div className="alert alert-danger" role="alert">
            this field is required!
        </div>
    }
}

const validEmail = (value) => {
    if (!isEmail(value)) {
        return (
        <div className="alert alert-danger" role="alert">
            This is not a valid email.
        </div>
        );
    }
} 

const validPassword = (value) => {
    if(value.length < 4 || value.length > 40){
        return(
            <div className="alert alert-danger" role="alert">
            The password must be between 4 and 40 characters.
            </div>
        )
    }
}

const validNom = (value) => {
    if(value.length < 4 || value.length > 20){
        return(
            <div className="alert alert-danger" role="alert">
            The name must be between 4 and 20 characters.
            </div>
        )
    }
}

const validPrenom = (value) => {
    if(value.length < 4 || value.length > 20){
        return(
            <div className="alert alert-danger" role="alert">
            The firstname must be between 4 and 20 characters.
            </div>
        )
    }
}

// Component
const Register = (props) =>{

    const form = useRef()
    const checkbtn = useRef()
    // string
    const [username, setUsername] = useState("")
    // string 
    const [password, setPassword] = useState("")
    // string 
    const [nom, setNom] = useState("")
    // string
    const [prenom, setPrenom] = useState("")
    // bool
    const [successFull, setSuccessFull] = useState(false)
    // string
    const [message, setMessage] = useState("")

    const onChangeUsername = (e) => {
        const username = e.target.value
        setUsername(username)
    }

    const onChangePassword = (e) => {
        const password = e.target.value
        setPassword(password)
    }

    const onChangeNom = (e) => {
        const nom = e.target.value
        setNom(nom)
    }

    const onChangePrenom = (e) => {
        const prenom = e.target.value
        setPrenom(prenom)
    }

    const handleRegister = (e) => {
        e.preventDefault()

        setMessage("")
        setSuccessFull(false)

        form.current.validateAll()

        if(checkbtn.current.context._errors.length === 0) {
            AuthService.register(username, password, nom, prenom).then(
                (response) => {
                    console.log(response.statusText)
                    setMessage(response.statusText)
                    setSuccessFull(true)
                    props.history.push("/infos")
                    window.location.reload()
                }, (error) => {
                    const resMessage = (error.response && 
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();

                    setMessage(resMessage)
                    setSuccessFull(false)
                    
                    
                }
            )
        }

        // props.history.push("/login")
        // window.location.reload()
    }

    return (
        <div className="col-md-12">
            <div className="card card-container">
                <Form onSubmit={handleRegister} ref={form}>
                    {!successFull && (
                        <div>
                            <div className="form-group">
                                <label htmlFor="username">Email</label>
                                <Input type="text" name="username" className="form-controle" 
                                value={username} 
                                onChange={onChangeUsername} 
                                validations={[required, validEmail]} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <Input type="password" name="password" className="form-controle" 
                                value={password} 
                                onChange={onChangePassword} 
                                validations={[required, validPassword]} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Nom</label>
                                <Input type="password" name="password" className="form-controle" 
                                value={nom} 
                                onChange={onChangeNom} 
                                validations={[required, validNom]} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="prenom">Prénom</label>
                                <Input type="prenom" name="prenom" className="form-controle" 
                                value={prenom} 
                                onChange={onChangePrenom} 
                                validations={[required, validPrenom]} />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary btn-block">Sign Up</button>
                            </div>
                        </div>  
                    )}
                

                    {message && (
                        <div className="form-group">
                            <div className={!successFull ? "alert alert-danger": "alert alert-success"} role="alert">
                                {message}
                            </div>
                        </div>
                    )}

                    <CheckButton style={{display: "none"}} ref={checkbtn} />
                </Form>
            </div>
        </div>
    )
}

export default Register