import React, { useEffect, useState } from 'react'
import { Switch, Route, Link } from "react-router-dom"
import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"

import Login from "./components/Connexion/Login"
import AuthService from './services/auth.service'
import Home from './components/Home/Home'
import Register from './components/Register/Register'
import Profile from './components/Profile/Profile'
import Page404 from './components/Page404/Page404'
import Infos from './components/Infos/Infos'
import BlockCompetences from './components/Infos/blocks/BlockCompetences'
import BlockExperiences from './components/Infos/blocks/BlockExperience'
import BlockFormations from './components/Infos/blocks/BlockFormations'
import BlockHobies from './components/Infos/blocks/BlockHobies'
import BlockLangues from './components/Infos/blocks/BlockLangues'
import BlockPermis from './components/Infos/blocks/BlockPermis'
import PageCv from './components/PageCv/PageCv'

const App = () => {

  const [showModeratorBoard, setShowModeratorBoard] = useState(false)
  const [showAdminBoard, setShowAdminBoard] = useState(false)
  const [currentUser, setCurrentUser] = useState(false)
  const user = AuthService.getCurrentUser() 

  useEffect(() => {
    

    if(user) {
      console.log(user)
      setCurrentUser(user)
      setShowModeratorBoard(user.roles.includes("ROLE_ADMIN"))
      setShowAdminBoard(user.roles.includes("ROLE_SUPER_ADMIN"))
    }
  }, [])

  const logout = () => {
    AuthService.logout()
  }

  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <Link to={"/"} className="navbar-brand">Accueil</Link>

        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/home"} className="nav-link">Home</Link>
          </li>
          {showModeratorBoard && (
            <li className="nav-item">
              <Link to={"/mod"} className="nav-link">Moderator Board</Link>
            </li>
          )}

          {showAdminBoard && (
            <li className="nav-item">
              <Link to={"/admin"} className="nav-link">Admin Board</Link>
            </li>
          )}

          {currentUser && (<>
            <li className="nav-item">
              <Link to={"/user"} className="nav-link">
                User
              </Link>
            </li>
            <li>
              <Link to={"/infos"} className="nav-link">
                Ajouter des infos
              </Link>
            </li>
            <li>
              <Link to={"/my-CV"} className="nav-link">
                voir mes CV
              </Link>
            </li>
            </>
          )}
        </div>

        {currentUser ? (
          <div className="navbar-nav ml-auto" >
            <li className="nav-item">
              <Link to={"/profile"} className="nav-link">{user.nom}</Link>
            </li>
            <li className="nav-item">
              <a href="/login" className="nav-link" onClick={logout}>Logout</a>
          </li>
        </div>
        ) : (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={"/login"} className="nav-link">Login</Link>
            </li>

            <li className="nav-item">
              <Link to={"/register"} className="nav-link">Sign Up</Link>
            </li>
          </div>
        )}
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/home"]} component={Helloworld} />
          <Route exact path="/login" component={Login} />
          <Route exact path={"/register"} component={Register} />
          <Route exact path={"/profile"} component={Profile} />
          <Route exact path={"/infos"} component={Infos} />
          <Route exact path={"/my-CV"} component={PageCv} />
          <Route exact path={"/infos/compétences"} >
            <Infos><BlockCompetences title="compétence"/></Infos>
          </Route>
          <Route exact path={"/infos/expériences"} >
            <Infos><BlockExperiences title="expériences"/></Infos>
          </Route>
          <Route exact path={"/infos/formations"} >
            <Infos><BlockFormations title="formations"/></Infos>
          </Route>
          <Route exact path={"/infos/Hobies"} >
            <Infos><BlockHobies title="Hobies"/></Infos>
          </Route>
          <Route exact path={"/infos/langues"} >
            <Infos><BlockLangues title="langues"/></Infos>
          </Route>
          <Route exact path={"/infos/Permis"} >
            <Infos><BlockPermis title="Permis"/></Infos>
          </Route>
          
          <Route path={"/user"} component={Home} />
          <Route path={"/mod"} component={Home} />
          <Route path={"/admin"} component={Home} />
          <Route component={Page404} />
        </Switch>
      </div>
    </div>
  );
}

const Helloworld = () => {
  return (
    <div>
      <h3>Hello world !</h3>
    </div>
  )
}

export default App



